**About this repo**

just repo contain old version of firefox mobile(android).

**Information about apks in this repo**

[fennec-68.9.0.multi.android-aarch64.apk](https://github.com/zcake3000/firefox-mobile-archive-apk/blob/main/fennec-68.9.0.multi.android-aarch64.apk) is official firefox 68.9 build for mobile aarch64 architecture.

[fennec-68.9.0.multi.android-arm.apk](https://github.com/zcake3000/firefox-mobile-archive-apk/blob/main/fennec-68.9.0.multi.android-arm.apk) is official firefox 68.9 build for mobile arm(arm32) architecture.

[fennec-68.9.0.multi.android-i386.apk](https://github.com/zcake3000/firefox-mobile-archive-apk/blob/main/fennec-68.9.0.multi.android-i386.apk) is official firefox 68.9 build for mobile i386(x86) architecture.

[fennec-68.9.0.multi.android-x86_64.apk](https://github.com/zcake3000/firefox-mobile-archive-apk/blob/main/fennec-68.9.0.multi.android-x86_64.apk) is official firefox 68.9 build for mobile x86_64(amd64) architecture.

[org.mozilla.fennec_fdroid_689420.apk](https://github.com/zcake3000/firefox-mobile-archive-apk/blob/main/org.mozilla.fennec_fdroid_689420.apk) is F-Droid build of firefox 68.12 (Fennec F-Droid).

[org.gnu.icecat_680412.apk](https://github.com/zcake3000/firefox-mobile-archive-apk/blob/main/org.gnu.icecat_680412.apk) is rebranded firefox 68.4.2 which it avilables on f-droid.(IceCatMobile)

**Source**

[fennec-68.9.0.multi.android-aarch64.apk](https://github.com/zcake3000/firefox-mobile-archive-apk/blob/main/fennec-68.9.0.multi.android-aarch64.apk) and [fennec-68.9.0.multi.android-arm.apk](https://github.com/zcake3000/firefox-mobile-archive-apk/blob/main/fennec-68.9.0.multi.android-arm.apk) and [fennec-68.9.0.multi.android-i386.apk](https://github.com/zcake3000/firefox-mobile-archive-apk/blob/main/fennec-68.9.0.multi.android-i386.apk) and [fennec-68.9.0.multi.android-x86_64.apk](https://github.com/zcake3000/firefox-mobile-archive-apk/blob/main/fennec-68.9.0.multi.android-x86_64.apk) is come from [firefox official ftp server](https://ftp.mozilla.org/pub/mobile/releases/68.9.0/)

[org.mozilla.fennec_fdroid_689420.apk](https://github.com/zcake3000/firefox-mobile-archive-apk/blob/main/org.mozilla.fennec_fdroid_689420.apk) is come from [f-droid(archived)](https://web.archive.org/web/20200914153140/https://f-droid.org/en/packages/org.mozilla.fennec_fdroid/)

[org.gnu.icecat_680412.apk](https://github.com/zcake3000/firefox-mobile-archive-apk/blob/main/org.gnu.icecat_680412.apk) is come from [f-droid](https://f-droid.org/en/packages/org.gnu.icecat/)
